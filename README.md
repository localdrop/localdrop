Farmdrop Multi-vendor Shop
===========================

Easy bootstrapping!
-------------------

Powered by the ubiquitous Makefile ... this should be pretty easy:

1. make deps or make deps_mac
2. make install
3. make run
4. open your browser to: http://127.0.0.1:35003

